import Logo from "./Logo";

const Header = () => {
  return (
    <header className="d-flex align-items-center mb-4">
      <Logo />
      <h1 className="mt-5">mtntrips</h1>
    </header>
  );
};

export default Header;
