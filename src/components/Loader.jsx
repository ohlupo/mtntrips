import { Spinner } from "react-bootstrap";

const Loader = () => {
  return (
    <>
      <Spinner size="lg" />
    </>
  );
};

export default Loader;
