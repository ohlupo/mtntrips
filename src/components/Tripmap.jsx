import Map, { FullscreenControl, Layer, Marker, Source } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";

let accessToken = import.meta.env.VITE_REACT_APP_MAP_TOKEN;

const Trip = ({ trip }) => {
  const geojson = {
    type: trip[0].features[0].geometry.type,
    coordinates: trip[0].features[0].geometry.coordinates,
  };

  const startingPoint = geojson.coordinates[0];
  const endPoint = geojson.coordinates[geojson.coordinates.length - 1];

  const latitude = trip[0].features[0].geometry.coordinates[0][1];
  const longitude = trip[0].features[0].geometry.coordinates[0][0];

  const layerStyle = {
    id: "line",
    type: "line",
    source: "route",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#4c6ef5",
      "line-width": 5,
    },
  };
  return (
    <div>
      <div>
        <Map
          initialViewState={{
            longitude,
            latitude,
            center: [latitude, longitude],
            zoom: 10,
          }}
          style={{ width: "100%", height: 400 }}
          mapboxAccessToken={accessToken}
          mapStyle="mapbox://styles/mapbox/streets-v9">
          <Marker
            color="#40c057"
            latitude={startingPoint[1]}
            longitude={startingPoint[0]}
          />
          <Marker
            color="#fa5252"
            latitude={endPoint[1]}
            longitude={endPoint[0]}
          />
          <Source id="my-data" type="geojson" data={geojson}>
            <Layer {...layerStyle} />
          </Source>
          <FullscreenControl />
        </Map>
      </div>
    </div>
  );
};

export default Trip;
