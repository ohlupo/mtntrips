import { VictoryChart, VictoryLine } from "victory";

const Tripelevation = ({ chartData }) => {
  return (
    <div>
      <VictoryChart>
        <VictoryLine data={chartData} />
      </VictoryChart>
    </div>
  );
};

export default Tripelevation;
