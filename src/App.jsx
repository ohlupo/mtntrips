import { Container } from "react-bootstrap";
import Header from "./components/Header";
import { Route, Routes } from "react-router-dom";
import Trip from "./pages/Trip";
import Trips from "./pages/Trips";

const App = () => {
  return (
    <Container>
      <Header />
      <Routes>
        <Route path="/trips/:name" element={<Trip />} />
        <Route path="/" element={<Trips />} />
      </Routes>
    </Container>
  );
};

export default App;
