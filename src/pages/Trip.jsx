import { Col, Row, Table } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import Tripmap from "../components/Tripmap";
import Tripelevation from "../components/Tripelevation";
import { length, lineString } from "@turf/turf";
import data from "../data/trips.json";

const Trip = () => {
  const { name } = useParams();

  const currentTrip = data.filter(
    (t) => t.features[0].properties.name === name
  );

  const { coordinates } = currentTrip[0].features[0].geometry;

  const convertedDistance = lineString(coordinates);

  const totalDistance = length(convertedDistance);

  const elevationArr = coordinates.map((coord) => {
    return coord[2];
  });

  const maxEl = Math.max(...elevationArr);
  const minEl = Math.min(...elevationArr);

  const chartData = currentTrip[0].features[0].geometry.coordinates.map(
    (x, i) => {
      return {
        x: i,
        y: x[2],
      };
    }
  );

  return (
    <>
      <Row className="mb-5">
        <Link to="/" className="nav-link">
          go back
        </Link>
      </Row>
      <Row>
        <Col>
          <h2>{name}</h2>
          <Table>
            <thead>
              <tr>
                <th>min elevation in meters</th>
                <th>max elevation in meters</th>
                <th>total distance in kilometers</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{minEl}</td>
                <td>{maxEl}</td>
                <td>{totalDistance.toFixed(2)}</td>
              </tr>
            </tbody>
          </Table>
        </Col>
      </Row>
      <Row>
        <Col>
          <Tripmap trip={currentTrip} />
        </Col>
      </Row>
      <Row my="10">
        <Col>
          <Tripelevation chartData={chartData} />
        </Col>
      </Row>
    </>
  );
};

export default Trip;
