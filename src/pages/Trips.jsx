import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Loader from "../components/Loader";
import data from "../data/trips.json";

const Trips = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading((loading) => !loading);
  }, []);

  return (
    <Container my="10">
      {data ? (
        data.map((trip, index) => (
          <Row key={index}>
            <Col>
              <Link
                className="nav-link"
                to={`/trips/${trip.features[0].properties.name}`}>
                {trip.features[0].properties.name}
              </Link>
            </Col>
          </Row>
        ))
      ) : <h2>no trips</h2> ? (
        isLoading
      ) : (
        <Loader />
      )}
    </Container>
  );
};

export default Trips;
